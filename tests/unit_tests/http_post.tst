libscicurl_Init();

curl = curl_easy_init();

curl_easy_setopt(curl, CURLOPT_URL, "http://httpbin.org/post");
  
curl_easy_setopt(curl, CURLOPT_POST, 1);
curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "name=scilab");

res = curl_easy_perform(curl);

curl_easy_cleanup(curl);

assert_checkequal(res, 0);
